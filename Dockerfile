FROM ubuntu
ENV VENV /opt/venv
LABEL version="1.0"
LABEL description="Creates an image of ubuntu with venv setup"
MAINTAINER PETER RAUHUT <baffling.bear@gmail.com>
RUN apt-get update && apt-get install -y python3-dev wget
WORKDIR /tmp
RUN wget https://bootstrap.pypa.io/get-pip.py
RUN python3 get-pip.py
RUN rm get-pip.py
RUN pip install virtualenv
RUN mkdir $VENV
WORKDIR $VENV
RUN virtualenv venv
